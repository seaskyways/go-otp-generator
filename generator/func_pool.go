package generator

import "sync/atomic"

type FuncPool struct {
	maxConcurrent uint
	running       atomic.Value
	queue         []func()
}

func NewFuncPool() *FuncPool {
	pool := new(FuncPool)
	pool.running.Store(uint(0))
	return pool
}

func (pool *FuncPool) Run(next func()) {
	pool.queue = append(pool.queue, next)
	pool.onUpdate()
}

func (pool *FuncPool) SetMaxConcurrent(maxConcurrent uint) {
	pool.maxConcurrent = maxConcurrent
	pool.onUpdate()
}

func (pool *FuncPool) onUpdate() {
	for pool.running.Load().(uint) <= pool.maxConcurrent &&
		len(pool.queue) > 0 {

		f := pool.queue[0]
		pool.queue = pool.queue[1:]
		pool.running.Store(
			pool.running.Load().(uint) + 1,
		)
		go func(f func()) {
			f()
			pool.running.Store(pool.running.Load().(uint) - 1)
		}(f)
	}
}
