package generator

import (
	"testing"
	"sync"
	"time"
)

func TestFuncPool_Run(t *testing.T) {
	t.Logf("Running 32 tasks")
	pool := NewFuncPool()
	pool.SetMaxConcurrent(2)
	wg := new(sync.WaitGroup)
	for i := 0; i < 32; i++ {
		wg.Add(1)
		func (i int) {
			pool.Run(func() {
				t.Logf("Hi from task %d", i)
				time.Sleep(time.Second)
				wg.Done()
			})
		}(i)
	}
	wg.Wait()
}
