package generator

import (
	"crypto/rand"
	"sync"
)

type CryptoLine []byte

type Generator struct {
	linePool sync.Pool
}

func NewGenerator() Generator {
	return Generator{
		linePool: sync.Pool{
			New: func() interface{} {
				return make([]byte, 256)
			},
		},
	}
}

func (g *Generator) GenCryptoLine() CryptoLine {
	randPool := g.linePool.Get().([]byte)
	defer g.linePool.Put(randPool)
	output := g.linePool.Get().([]byte)
	defer g.linePool.Put(output)

	g.FillCryptoLine(output, randPool)

	return output
}

func (_ *Generator) FillCryptoLine(
	output []byte,
	randPool []byte,
) CryptoLine {

	for i := range output {
		output[i] = byte(i % 256) // fill input with bytes from 0 to len(input) - 1
	}

	rand.Read(randPool)

	// Start filling the output using a random index and boundary
	for bound := len(output) - 1; bound > 0; bound-- {
		randIndex := int(randPool[bound])
		position := randIndex % bound
		output[bound], output[position] = output[position], output[bound]
	}

	return output
}

func (g *Generator) GenCryptoFile(size int) []CryptoLine {
	file := make([]CryptoLine, size)
	for i := range file {
		file[i] = g.GenCryptoLine()
	}

	return file[:]
}

func Remove(s []byte, i int) []byte {
	s[len(s)-1], s[i] = s[i], s[len(s)-1] // swap target with last position
	return s[:len(s)-1]                   // cut from start to before last by one
}
