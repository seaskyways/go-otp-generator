package generator

import (
	"testing"
	"math/rand"
	"strconv"
	"sync"
	"math"
	"bytes"
	"github.com/google/btree"
	"github.com/cespare/xxhash"
	"time"
)

func BenchmarkGenerator_GenCryptoLine(b *testing.B) {
	g := NewGenerator()
	start := time.Now()
	num := 10000
	wg := new(sync.WaitGroup)
	wg.Add(num)
	for i := num; i >= 0 ; i-- {
		go func(i int) {
			g.GenCryptoLine()
			wg.Done()
		}(i)
	}
	wg.Wait()
	end := time.Since(start)
	b.Logf("%d lines took %f.3ms", num, end.Seconds()*1000)
}

func TestGenCryptoLine(t *testing.T) {
	g := NewGenerator()
	byteArray := g.GenCryptoLine()
	t.Log("Generated array")
	t.Log(byteArray)

	t.Log("Check for uniqueness")

	unique := true
	for i1, e1 := range byteArray {
		for i2, e2 := range byteArray {
			if i1 == i2 {
				continue
			}
			if e1 == e2 {
				unique = false
				break
			}
		}
	}

	if !unique {
		t.Fatal("Array is not of unique elements")
	} else {
		t.Log("Array is unique")
	}

}

func TestRemove(t *testing.T) {
	a := []byte{0, 1, 2}
	a = Remove(a, 1)
	if !bytes.Equal(a, []byte{0, 2}) {
		t.Fatal("Remove did not work")
	}
}

func TestGenCryptoFile(t *testing.T) {
	g := Generator{}
	t.Log("Started generation")
	start := time.Now()
	file := g.GenCryptoFile(168000)
	duration := time.Since(start)
	t.Logf("Finished generation, time elapsed %.3fs", duration.Seconds())

	t.Log("Check line uniqueness")

	start = time.Now()

	degree := int(math.Log(float64(len(file))) * 3.5)
	bTree := btree.New(degree)

	xxh := xxhash.New()
	for _, slice := range file {
		xxh.Write(slice)
		hash := xxh.Sum64()
		existing := bTree.ReplaceOrInsert(btree.Int(hash))
		if existing != nil {
			t.Error("Found duplicate hash", existing)
		}
	}

	duration = time.Since(start)
	t.Logf("Finished uniqueness check, time elapsed %.3fs", duration.Seconds())
}

func Fact(n uint64) uint64 {
	var r uint64 = 1
	for i := n; i >= 2; i-- {
		r *= i
	}
	return r
}

func TestFactorial(t *testing.T) {
	t.Log("Factorial 2 = " + strconv.Itoa(int(Fact(2))))
	t.Log("Factorial 3 = " + strconv.Itoa(int(Fact(3))))
	t.Log("Factorial 4 = " + strconv.Itoa(int(Fact(4))))
}

func TestShuffleSmall(t *testing.T) {
	const N = 6
	var original = make([]byte, N)
	for i := 0; i < N; i++ {
		original[i] = byte(i)
	}
	fact := Fact(uint64(N))
	bits := math.Log(float64(fact)) / math.Log(2)
	pCount := int(math.Ceil(math.Pow(2, bits)))

	var permutations = make([][]byte, pCount)
	var wg = new(sync.WaitGroup)
	for i := range permutations {
		wg.Add(1)
		go func(seed int) {
			defer wg.Done()

			var random = rand.New(rand.NewSource(int64(seed) + 1))
			permutations[seed] = make([]byte, N)
			copy(permutations[seed], original)

			random.Shuffle(N, func(i, j int) {
				permutations[seed][i], permutations[seed][j] = permutations[seed][j], permutations[seed][i]
			})
		}(i)
	}
	wg.Wait()

	var occurrences = make(map[string]int)
	for _, permutation := range permutations {
		//for s2, p2 := range permutations {
		//	if s2 == seed {
		//		continue
		//	}
		//	if bytes.Equal(permutation, p2) {
		//		//t.Error("Bytes found colliding at seeds : ", seed, s2, permutation, p2)
		//		continue outer
		//	}
		//}
		if i, ok := occurrences[string(permutation)]; ok {
			occurrences[string(permutation)] = i + 1
		} else {
			occurrences[string(permutation)] = 1
		}
	}
	var unique, nonunique = 0, 0
	unique = len(occurrences)

	nonunique = int(fact) - unique

	t.Log("Unique : ", unique)
	t.Log("Non-Unique : ", nonunique)
	t.Log(occurrences)
}
