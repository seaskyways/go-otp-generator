package main

import (
	"io/ioutil"
	"go-otp-generator/generator"
	"flag"
)

func main() {
	fileName := flag.String("out", "", "File name to output dictionary")
	flag.Parse()

	otpGen := new(generator.Generator)
	lines := otpGen.GenCryptoFile(168000)
	data := make([]byte, 1680000*256)
	for _, b := range lines {
		data = append(data, b...)
	}
	ioutil.WriteFile(*fileName, data, 777)
}