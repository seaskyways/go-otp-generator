package main

import (
	"crypto/rand"
	"io"
)

type OTPPool struct {
	randReader io.Reader
	maxSize    int
	pool       []byte
}

func New(maxSize uint) *OTPPool {
	p := new(OTPPool)
	p.maxSize = int(maxSize)
	p.pool = make([]byte, 0, maxSize)
	p.randReader = rand.Reader

	go p.generate()

	return p
}

func (p *OTPPool) SetMaxSize(n uint) {
	p.maxSize = int(n)
	p.pool = p.pool[:n]
}

func (p *OTPPool) generate() {
	buf := make([]byte, 1024)
	for len(p.pool) != p.maxSize {
		n, _ := p.randReader.Read(buf)
		p.pool = append(p.pool, buf[:n]...)
	}
}

func main() {

}
