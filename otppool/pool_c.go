package main

//
import "C"

//export IOTPPool
type IOTPPool interface {
	SetMaxSize(n uint)
	generate()
}

//export NewPool
func NewPool(maxSize uint) IOTPPool {
	return New(maxSize)
}

//export SetMaxSize
func SetMaxSize(p IOTPPool, n uint) {
	p.SetMaxSize(n)
}
