package main

import (
	"fmt"
	"testing"
	"time"
)

func TestNew(t *testing.T) {
	p := New(1024 * 1024 * 1024)
	for len(p.pool) != p.maxSize {
		printPool(p)
		time.Sleep(time.Millisecond * 1000)
	}
	printPool(p)
}

var preLen = 0
var diffLen = 0

func printPool(p *OTPPool) {
	diffLen = len(p.pool) - preLen
	fmt.Printf(
		"PoolLength=%d MB\tCapacity=%d MB\tPercentrage=%.2f\tSpeed=%.2f MB/s\n",
		len(p.pool)/(1024*1024),
		cap(p.pool)/(1024*1024),
		float64(len(p.pool)*100)/float64(cap(p.pool)),
		(float64(diffLen) / (1024.0 * 1024.0)),
	)

	preLen = len(p.pool)
}
